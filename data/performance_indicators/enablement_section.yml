- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: 1.3
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - Page load times have decreased from their peak in April, however still far above our target which we achieved in the winter. 
  implementation:
    status: Complete
    reasons:
      - We are still working to [improve our per-geo views](https://gitlab.com/gitlab-data/analytics/-/issues/8014).
  lessons:
    learned:
    - We have tried to [associate URL's to specific workflows](https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=10888150&udv=0), however were only able to attribute about 50% of our page loads. We need a product change to [directly tie page load data to routes](https://gitlab.com/gitlab-org/gitlab/-/issues/331807). This will make the data more directly actionable and reliable. However there are insights to be gained. Of the attributed routes, it is clear Issue and MR loading times are represent significantly more of our load time (in MS) than page hits. This indicates these workflows are slower than our average load time, and frequently used. These should be focus areas: MR speed is an OKR this quarter, but we should also examine Issue loading speed.
    - While we have our [LCP Leaderboard](https://dashboards.gitlab.net/d/sftijGFMz/sitespeed-lcp-leaderboard?orgId=1&refresh=30s) for frontend performance, this requires humans to look at the dashboard and look for changes or hotspots. This does not work well at scale. We need to add [frontend performance to error budgets](https://gitlab.com/gitlab-com/Product/-/issues/2428) which we are working on this quarter, as well as work towards a [CI test suite](https://gitlab.com/gitlab-org/gitlab/-/issues/331808) to catch regressions before they make it to production.
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
  metric_name: performanceTiming
  sisense_data:
    chart: 10546836
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - Age seems to have leveled off, % on latest 3 versions begins to climb after low January numbers.
    - Slightly positive month across the board, need to continue to analyze possible reasons to keep momentum going.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - This month again appears to be different from the expected trend and the trend continues [to level off or decrease](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380) in median age of version for all versions and license plans. This [will continue to be investigated](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/821) to see how we can use these results to accelerate a downward trend. One conclusion to note, the largest decrease in trend occurs with EE and Ultimate customers, and this [will be further researched](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/844), to see if outreach differs at all between different licenses and plans.
    - The trend for [percentage on the latest 3 versions](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8890933&udv=1059380) seems to be leveled off, or 1 point up for different install types. The helm install is still well above the target of 40, and the other install types continue to slightly move towards 40. Perhaps this bottom of the dip was due to low upgrade rate around/after the holiday season in January. 
    - Security was a central focus for admins to upgrade their version from the [ease of upgrading study](https://gitlab.com/gitlab-org/ux-research/-/issues/1114). Based on that insight, getting users to sign up for the [security newsletter](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/842) might be a good MVC for [new ways to prompt users to upgrade](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/843).
    - Another important aspect for admins to upgrade was ["when developers are asleep"](https://gitlab.com/gitlab-org/ux-research/-/issues/1114#note_515385825), or less active within an organization. We can research how to [suggest best times to upgrade](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/845) to admins, and begin to understand those times.
    - Work has begun on [better highlighting](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators. This will impact results once it has been completed, estimated for this milestone.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Distribution  - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-distribution/stage-groups-group-dashboard-enablement-distribution)

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) to the usage data. These metrics are not yet available; we are working on getting them plotted in Sisense. We also intend to add tracking for [git push operations on a Geo secondary](https://gitlab.com/gitlab-org/gitlab/-/issues/320984). This work is currently paused due to capacity constraints.
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we will begin tracking Git pulls in 13.11 with tracking for Git pushes planned as part of the [Geo Usage Ping Epic](https://gitlab.com/groups/gitlab-org/-/epics/4660). We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. We've recently completed a PoC to make the secondary UI indistinguishable from the primary. Implementation work is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster, but if one occurs everyone benefits. Based on [the node number distribution](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=6471733&udv=0) ~60% of our customers use Geo mainly for DR. I think this is worth measuring because setting up Geo is always a conscious decision by the customer - it must be configured.
    - We are working to add support for replicating all data types, so that Geo is on a solid foundation for both DR and Geo Replication.
    - In 13.12 we replicate ~86% of all data types (25 out of 29 in total) and verify ~48%. Shipped [Terraform State File verification](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#geo-verifies-replicated-terraform-state-files)
 
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - The number of users is still very low, likely because the WebUI is insufficient. We are planning to change this, see [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing) and in-progress implementation work in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    - A bump in month over month growth with [10.7.% increase in new Geo customers from March to April](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=9939914&udv=0). We also hit more than 500 reported Geo nodes for the first time. Up from 461 - another ~10.6% growth month over month. May data still incomplete.
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2

- name: Enablement:Geo - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-geo/stage-groups-group-dashboard-enablement-geo)
    - Geo is not deployed on .com - This currently monitors one sidekiq worker that checks whether Geo is enabled.

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
    - A bug in the plot displays the versions out of order (13.11 next to 13.1). Investigating a fix.
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption has stabilized between 13.11 and 13.12
    - We are currently working on reducing [the memory consumption for Puma and Sidekiq](https://gitlab.com/groups/gitlab-org/-/epics/5622). [Analysis by the team](https://gitlab.com/groups/gitlab-org/-/epics/5622#note_573391275) indicates a number of inefficiencies, for example inefficient Git integrations or markdown rendering. Working to increase overall visibility of memory consumption, for example by [highlighting it in the developer performance bar](https://gitlab.com/gitlab-org/gitlab/-/issues/330736).
    - To further reduce memory consumption, we are planning to work on [splitting the application into functional parts to ensure that only needed code is loaded](https://gitlab.com/groups/gitlab-org/-/epics/5278) and on [reducing the memory consumption for Puma and Sidekiq endpoints](https://gitlab.com/groups/gitlab-org/-/epics/5622).

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - In April, Paid GMAU is down by 2%.   SaaS down -2.5%, Self-Manged up 2.5% 
    - Self-Manged continues to grow, We contiune to see increased interest in code search of existing very larger customers
    - Perfromance has been improved in SaaS, We have increased from 10 to 12 nodes and decreased the number of calls in the Result page. 
    - Contiuneing to grow Paid GMAU will likely come down to New UI features and improvements in Advanced Search. This will expand the use cases for GitLab users making it a more viable option for more personas. 
  implementation:
    status: Complete
    reasons:
    - Data collected is incomplete for September and October 2020 and January 2021
  lessons:
   learned:
    - Several Large Customers had not enabled advanced search becasue the Elastic Cluster creditionals were visible on the Admin Page. [We have encrypted and masked the password in 13.12.](https://gitlab.com/gitlab-org/gitlab/-/issues/328466) 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Global Search - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search)
    - Error Budget was Exceeded for Global Search in April 2021. This will need to be investigated to determine why and what needs to be improved. https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1079    

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - The query Apdex has decreased in all charts after the release of 13.10, including [the weekly one](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=9885649&udv=0), which has a pretty constant number of instances reporting stats. No similar trend on the GitLab.com charts, nor any other clear signal indicating what may be affecting the Apdex. This is still present in 13.11 but the team has not focused on an investigation due to capacity constraints. Current focus is mitigating the Primary Key Overflow risk.
    - We expect the updates performed in the scope of [Research Database queries for performance and frequency](https://gitlab.com/groups/gitlab-org/-/epics/5652) to further improve the ratio of queries which complete within 250ms and lower the variance of the Apdex even further.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/64e6acku) and [50ms - Tolerable 100ms](https://tinyurl.com/4mjw5azv) for master) but reflects with sharp drops the production incidents that related to the database.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Database - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-database/stage-groups-group-dashboard-enablement-database)    

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 134000
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Paid Monthly Active Users on GitLab.com increased by 3.8% in April to 124K surpassing the reset Q1 target of 122K.
    - Added Q2 Target
    - Improvement - We are continuing to focus on improving uptime and stability of GitLab.com via the InfraDev process, Corrective Action items, and continued rollout of error budgets with stage groups. Additionally, we are tracking key features needed to achieve [Enterprise Readiness on GitLab.com](https://gitlab.com/groups/gitlab-org/-/boards/2037713) and extending our [compliance posture](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/2505) on Gitlab.com.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Need to account for seasonal fluctuations into target projections. 
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2

- name: Enablement:Infrastructure - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-infrastructure/stage-groups-group-dashboard-enablement-infrastructure)   
