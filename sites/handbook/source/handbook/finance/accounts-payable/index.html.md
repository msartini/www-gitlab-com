---
layout: handbook-page-toc
title: "Accounts Payable"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
  {:toc .hidden-md .hidden-lg}

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to the Accounts Payable page! You should be able to find answers to most of your questions here. If you can't find what you are looking for, then:

- **Chat Channel**: `#accountspayable`
- **Email**: `ap@gitlab.com`

## <i class="fas fa-stream" id="biz-tech-icons"></i> QuickLinks

<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/finance/accounting/#procure-to-pay/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Invoicing and Payment</span></a>  
  <a href="/handbook/finance/accounting/#credit-card-use-policy/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Corporate Credit Card</span></a>  
  <a href="/handbook/finance/procurement/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Procurement</span></a>
</div>

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> General Guidelines

- Payment Runs are completed on Thursdays. Invoices that are paid on Thursday must be fully approved and vendors successfully onboarded by Wednesday morning of that week to be included in Thursday's payment run.

- Invoices are paid strictly by the invoice due dates. Any "Urgent Payment" requests will be paid in the next available payment run as long as they have been fully approved and the vendor has been successfully onboarded. AP cannot accommodate same day payment requests.

- Suppliers are to be paid via ACH and Wire only. AP does not issue check payments.

- Cutoff for customer refund payments are the 25th of each month. Any customer refund requests received after the 25th will be paid the following month.

- AP team will only create issues that are AP related in nature. Any item forwarded to AP that is outside of the scope of the AP team will be forwarded back to the requestor to handle or create the issue.

- Contractors and consultants who invoice biweekly or monthly will be set to the industry standard of 30 day payment terms.

## <i class="far fa-question-circle" id="biz-tech-icons"></i> Frequently Asked Questions

1. My invoice has not been paid yet, and my vendor is asking me where their money is.

- Check if your issue was fully approved. AP cannot send an invoice out for approval until ALL approval boxes are checked off in the procurement issue and the contract has been uploaded.
- Check if the invoice was sent to the correct place - an invoice uploaded to an issue will NOT get processed until it is emailed to gitlab@supplierinvoices.com or ap@gitlab.com
- Ensure that your vendor has fully onboarded themselves in Coupa or Tipalti.
- Ensure that your vendor has invoiced to the correct GitLab entity. AP cannot process an invoice unless it is addressed to the entity selected in the issue or PO. If a new invoice is required, AP will reach out to the issue/PO owner and request that they obtain a new invoice from the vendor.
- Further more, AP cannot process an invoice unless the correct VAT has been included on the invoice - per the entity that the vendor is invoicing to.
  If you are sure that all of these things have been done correctly, please ask the vendor to contact the AP team directly, via email _ap@gitlab.com_

2. I do not know which system to push my vendor to.

- Any vendors for Inc, Federal, IT BV and BV will be processed with a PO in Coupa as of June 2021.
- All other entities will continue to be processed and paid through Tipalti. This includes Ireland, GK, GmbH, Korea, UK, Canada Corp, Singapore

3. How do I submit an expense report and when will it get paid?

- All questions about expenses can be answered on the [Expense page](/handbook/finance/expenses/).
- Reports are paid out on varying timelines depending on which country you are located in. This is also detailed on the Expense page.

## <i class="far fa-flag" id="biz-tech-icons"></i> Coupa is Coming!

We’re excited to announce that GitLab will be launching Coupa in June!

Coupa is a procure-to-pay system that will help us streamline the purchase request process, initiate workflow with approvals, and enable Purchase Orders. We will be rolling out in a phased approach, starting with the US and Netherlands entities - GitLab Inc, Federal, IT BV and BV.

You can learn more about Coupa in our [FAQ Page](/handbook/finance/procurement/coupa-faq/)

## Need to get an invoice paid?

GitLab currently uses procurement issues as the first step in the process.
Next a vendor will be set up in our invoice processing and payment system.
Further details on this process can be found at [the Procure to Pay page](handbook/finance/accounting/#procure-to-pay)

## Expenses

Please review the page on [Spending Company Money](/handbook/spending-company-money/) to ensure that your spend is within the company [Expense policy](/handbook/finance/expenses/).

GitLab employs a external company called Montgomery Pacific - commonly known as Montpac. They help us to audit and approve / reject expenses according to the Expense Policy.

If your report has been rejected my Montpac or Accounts Payable, please reach out to your manager to discuss further.
