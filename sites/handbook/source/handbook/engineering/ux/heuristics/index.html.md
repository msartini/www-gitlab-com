---
layout: handbook-page-toc
title: "UX Heuristics"
description: "Heuristics used by the UX team to evaluate our product."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro
UX Heuristics are guidelines we can use to grade experiences in our product. They can also be useful while designing because you can use them to make design decisions or identify parts of your design that still need work.

This list can change and the handbook version is the SSOT. It's based on best practices in the UX field, as well as feedback from our customers as to what they want their GitLab experience to be like.

## Templates
* [Competitor Evaluation](https://docs.google.com/spreadsheets/d/1rtJV4a3IW-2jpq4Ifz3GBKcT3xikmZRecQVeBGJ3yz8/edit#gid=0)
* [UX Scorecard Spreadsheet](https://docs.google.com/spreadsheets/d/1Z_70fDg578wu8j_kta3vSLOHWv-BEJ1zuqniyBQUTeI/edit?usp=sharing)
* [UX Scorecard Issue Template](https://gitlab.com/gitlab-org/gitlab-design/-/issues/new?issuable_template=UX%20Scorecard%20Part%201)

## Heuristics

| Category | Heuristic | Description | 
| ------ | ------ | ------ |
| Usability | Visibility of system status | Communicating the current state allows users to feel in control of the system, take appropriate actions to reach their goal, and ultimately trust the brand. (source: NN/g) |
| Usability | Flexibility of use | Shortcuts (unseen by the novice user) speed up the interaction for the experts such that the system can cater to inexperienced and experienced users. (source: NN/g) |
| Usability | User control and freedom | Allow people to exit a flow or undo their last action and go back to the system’s previous state. (source: NN/g) |
| Usability | Recognize, diagnose, recover from errors | Make error messages visible, reduce the work required to fix the problem, and educate users along the way. (source: NN/g) |
| Laws and tenets | Protective: I don’t lose my data | (source: uitraps.com) |
| Laws and tenets | Forgiving: I can undo my actions | (source: uitraps.com) |
| Laws and tenets | Understandable: I know what I can do | (source: uitraps.com) |
| Laws and tenets | Efficient: I take fewer steps and process less information | (source: uitraps.com) |
| Characteristics | Real-time user interface | (see: [The future of MRs: Real-time collaboration](https://about.gitlab.com/blog/2019/12/19/future-merge-requests-realtime-collab/)) |
| Characteristics | A tool the whole team can use | (see: [GitLab Direction #personas](https://about.gitlab.com/direction/#personas)) |
| Characteristics | Minimal setup required | (see: [GitLab Product Principles #convention-over-configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration)) |
| Characteristics | For technical areas, documentation is easy to use |   |
| Onboarding | Learnability and support for new users |   |
| Onboarding | Features communicate the problem being solved/JTBD and value to a new user | When users explore an unfamiliar feature, they should be able to quickly understand how it is or isn't relevant to them. |
| Onboarding | Tasks are easy to learn, or if highly complex, have detailed in-app guidance | Tasks should be intuitive first and foremost, and when they are complex, they must provide contextual support for quick understanding. |
| Onboarding | Features/workflows have clear calls to action and provide guidance on how to get started quickly | A call to action should make it clear what is to be done to move the task forward. |
| Onboarding | Onboarding guidance carries through from an empty state to the moment of value | At the least, provide links to documentation. At best, provide in-app contextual support, tutorials, templates or wizards. |
| Onboarding | Time to experience value is measurable and optimized | This heuristic is a bit quantitative. To score high, your workflow must be instrumented such that you can observe users start to finish, and the workflow should have been optimized to have as few steps as possible. |


## Scoring
Scoring these heuristics can have an element of subjectivity, but the more you involve other users in the evaluation the less subjective it will be. At the least, the product designer can use the following scale to assess how well the heuristic is met.

| Badge | Score | Summary | Description |
| --- | --- | --- | --- |
| A | 5 | Excellent | The heuristic is met in every way and optimized. Future improvements are minor. Users may even report that completing the task is delightful. Users report exceeded expectations and high confidence of task success.|
| B | 4 | Good | The heuristic is met in almost every way, with no more than minor exceptions. The task is streamlined, pleasant and highly usable. Users may report exceeded expectations and medium high - high confidence in task success. |
| C | 3 | Average | The heuristic is mostly met. Exceptions could require minor workarounds, but the completing the task is relatively straight forward and overall meets expectations. Users may report only moderate confidence in task success.  |
| D | 2 | Below Average | The heuristic is lacking in significant ways leading to frustration. The task requires workarounds to complete or otherwise does not meet expectations. Users report low confidence in task success. |
| F | 1 | Bad | The heuristic isn't met. It may be completely ignored, or so poorly implemented that the task is impossible to complete.  |
| - | - | Unknown | This task hasn't been scored. |



## References
- [Nielssen Norman Group](https://www.nngroup.com/articles/ten-usability-heuristics/)
- https://uitraps.com/

